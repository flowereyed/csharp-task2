﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace csharptask_2_2
{
    class Program
    {
        static void Main()
        {
            var watch = new Stopwatch();
            ArrayList arrayList = new ArrayList();
            LinkedList<int> linkedList = new LinkedList<int>();

            // Adding an element
            watch.Start();
            for (int i = 0; i < 10000; i++)
            {
                arrayList.Add(i);
            }
            watch.Stop();
            Console.WriteLine("Adding elements to: ");
            Console.WriteLine("ArrayList: " + watch.Elapsed.TotalMilliseconds + " ms");

            if (!watch.IsRunning) watch.Restart();
            for (int i = 0; i < 10000; i++)
            {
                linkedList.AddLast(i);
            }
            watch.Stop();
            Console.WriteLine("LinkedList: " + watch.Elapsed.TotalMilliseconds + " ms");

            // Searching for an element
            const int Number = 5000;

            if (!watch.IsRunning) watch.Restart();
            bool found = arrayList.Contains(Number);
            watch.Stop();
            Console.WriteLine("\nSearching for an element in: ");
            Console.WriteLine("ArrayList (found: " + found + "): " + watch.Elapsed.TotalMilliseconds + " ms");

            if (!watch.IsRunning) watch.Restart();
            found = linkedList.Contains(Number);
            watch.Stop();
            Console.WriteLine("LinkedList (found: " + found + "): " + watch.Elapsed.TotalMilliseconds + " ms");

            // Deleting an element
            if (!watch.IsRunning) watch.Restart();
            arrayList.Remove(Number);
            watch.Stop();
            Console.WriteLine("\nRemoving an element from: ");
            Console.WriteLine("ArrayList: " + watch.Elapsed.TotalMilliseconds + " ms");

            if (!watch.IsRunning) watch.Restart();
            linkedList.Remove(Number);
            watch.Stop();
            Console.WriteLine("LinkedList: " + watch.Elapsed.TotalMilliseconds + " ms");
        }
    }
}