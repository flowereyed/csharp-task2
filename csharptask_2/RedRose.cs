﻿namespace csharptask_2
{
    public class RedRose : ThornedFlower
    {
        public RedRose() : base(ThornAmount.Low, "Red Rose", FlowerColor.Red, 2.30)
        { }
    }
}