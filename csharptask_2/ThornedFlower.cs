﻿namespace csharptask_2
{
    public abstract class ThornedFlower : Flower
    {
        protected ThornAmount Amount { get; set; }

        protected ThornedFlower(ThornAmount amount, string name, FlowerColor color, double price) 
            : base (name, color, price)
        {
            Amount = amount;
        }

        protected ThornedFlower() : base ()
        {
        }

        public override string ToString()
        {
            return base.ToString() + "Thorns Amount: " + Amount + "\n";
        }
    }
}