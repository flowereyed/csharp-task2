﻿namespace csharptask_2
{
    public class WhiteRose : ThornedFlower
    {
        public WhiteRose() : base(ThornAmount.Medium, "White Rose", FlowerColor.White, 2.50)
        { }
    }
}