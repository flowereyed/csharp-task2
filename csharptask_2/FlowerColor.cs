﻿namespace csharptask_2
{
    public enum FlowerColor
    {
        Blue,
        Yellow,
        Red,
        Purple,
        White
    }
}