﻿namespace csharptask_2
{
    public class Camomile : Flower
    {
        public Camomile() : base("Camomile", FlowerColor.White, 1.50)
        { }
    }
}