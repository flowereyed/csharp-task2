﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharptask_2
{
    class Bouquet
    {
        private Dictionary<string, FlowerItem> flowerMap;

        public Bouquet()
        {
            flowerMap = new Dictionary<string, FlowerItem>();
        }

        public void AddFlower(Flower flower)
        {
            if (flowerMap.ContainsKey(flower.Name))
            {
                flowerMap[flower.Name].IncrementAmount();
            } 
            else
            {
                flowerMap.Add(flower.Name, new FlowerItem(flower));
            }
        }

        public void Clear()
        {
            flowerMap.Clear();
        }

        public double CalculatePrice()
        {
            double price = 0;

            foreach (FlowerItem item in flowerMap.Values)
            {
                price += item.Flower.Price * item.Amount;
            }
            return price;
        }

        public override string ToString()
        {
            StringBuilder bouquet = new StringBuilder();

            foreach (FlowerItem item in flowerMap.Values)
            {
                bouquet.Append(item.ToString());
            }
            bouquet.Append("Total price: " + CalculatePrice());
            return bouquet.ToString();
        }
    }

    class FlowerItem
    {
        public Flower Flower { get; set; }
        public int Amount { get; set; }

        public FlowerItem(Flower flower)
        {
            Flower = flower;
            Amount = 1;
        }

        public override string ToString()
        {
            return Flower.ToString() + "\nAmount: " + Amount + "\n\n";
        }

        public void IncrementAmount()
        {
            Amount++;
        }

        public void DecrementAmount()
        {
            Amount--;
        }
    }
}