﻿using System;

namespace csharptask_2
{
    class Program
    {
        private static readonly string MENU_LINE = "****************************";
        private const char CHOOSE_FLOWER_SYMBOL = '1';
        private const char PRINT_BOUQUET_SYMBOL = '2';
        private const char CLEAR_BOUQUET_SYMBOL = '3';
        private const char QUIT_SYMBOL = '4';

        private static Bouquet bouquet = new Bouquet();

        static void Main()
        {
            char userAnswer;

            do
            {
                PrintMenu();
                userAnswer = Console.ReadKey().KeyChar;

                switch (userAnswer)
                {
                    case CHOOSE_FLOWER_SYMBOL:
                        ChooseFlower();
                        break;
                    case PRINT_BOUQUET_SYMBOL:
                        PrintBouquet();
                        break;
                    case CLEAR_BOUQUET_SYMBOL:
                        ClearBouquet();
                        break;
                }
            }
            while (userAnswer != QUIT_SYMBOL);
        }

        private static void ChooseFlower()
        {
            Console.Clear();
            PrintFlowerChooseMenu();
            char userAnswer = Console.ReadKey().KeyChar;

            switch (userAnswer)
            {
                case '1':
                    bouquet.AddFlower(new RedRose());
                    break;
                case '2':
                    bouquet.AddFlower(new WhiteRose());
                    break;
                case '3':
                    bouquet.AddFlower(new Camomile());
                    break;
            }

            Console.Clear();
        }

        private static void PrintFlowerChooseMenu()
        {
            Console.WriteLine(MENU_LINE);
            Console.WriteLine("1. Red Rose");
            Console.WriteLine("2. White Rose");
            Console.WriteLine("3. Camomile");
            Console.WriteLine("Press any key to quit");
            Console.WriteLine(MENU_LINE);
        }

        private static void PrintBouquet()
        {
            Console.Clear();
            Console.WriteLine(bouquet);
            Console.WriteLine("Press any key to quit");
            Console.ReadKey();
            Console.Clear();
        }

        private static void ClearBouquet()
        {
            Console.Clear();
            bouquet.Clear();
            Console.WriteLine("Bouquet was succefully cleared.\nPress any key to quit.");
            Console.ReadKey();
            Console.Clear();
        }

        private static void PrintMenu()
        {
            Console.Clear();
            Console.WriteLine(MENU_LINE);
            Console.WriteLine("1. Choose flower");
            Console.WriteLine("2. Print bouquet");
            Console.WriteLine("3. Clear bouquet");
            Console.WriteLine("4. Quit");
            Console.WriteLine(MENU_LINE);
        }
    }
}