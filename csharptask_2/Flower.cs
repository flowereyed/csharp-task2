﻿namespace csharptask_2
{
    public abstract class Flower
    {
        public string Name { get; set; }
        public FlowerColor Color { get; set; }
        public double Price { get; set; }

        protected Flower()
        {
        }

        protected Flower(string name, double price)
        {
            Name = name;
            Price = price;
        }

        protected Flower(string name, FlowerColor color, double price)
        {
            Name = name;
            Color = color;
            Price = price;
        }

        public override string ToString()
        {
            return "Name: " + Name + "\n" + "Color: " + Color + "\n" + "Price: " + Price + "\n";
        }
    }
}